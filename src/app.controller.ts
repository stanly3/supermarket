import { Req, Body, Controller, Get, Render, Post, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Request, Response } from 'express';

@Controller()
export class AppController {
  private message: string;
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  root() {
    return { displayMessage: this.message };
  }

  @Post('scan')
  scan(@Req() req: Request, @Res() res: Response): void {
    this.message = req.body.sku;
    res.redirect('/');
  }
}
