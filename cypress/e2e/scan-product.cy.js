/// <reference types="cypress" />

// Welcome to Cypress!
// https://on.cypress.io/introduction-to-cypress

describe('example to-do app', () => {
  it('displays two todo items by default', () => {
    cy.visit('http://localhost:3000');
    cy.get('[data-cy="sku"]').type(`hello{enter}`);
    cy.get('[data-cy="display"]').should('have.text', 'hello');
  });
});
